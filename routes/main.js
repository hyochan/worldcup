module.exports = function(app, io) {
    var userCtrl = require('../ctrl/userCtrl.js');
    app.get('/user', function(req, res) {
        userCtrl.getUsers(req, res, io);
    });

    app.get('/server', function(req, res) {
        userCtrl.getAllUsers(req, res, io);
    });

    app.get('/all' , function(req, res) {
        userCtrl.getAllData(req, res, io);
    });
}