var async = require('async');
var db = require('../util/tables.js');

var messages = [];
var sockets = [];

exports.getAllUsers = function(req, res, io) {
    res.render('admin.html');
    io.on('connection', function (socket) {
        if (sockets.indexOf(socket) < 0) {
            console.log('admin connection socket:'+socket);
            sockets.push(socket);
            getAllData();
        }

        socket.on('disconnect', function () {
            if (sockets.indexOf(socket) >= 0) {
                sockets.splice(sockets.indexOf(socket), 1);
            }
        });
    });
};

exports.getUsers = function(req, res, io) {

    res.render('client.html');
    io.sockets.on('connection', function (socket) {
        var dbCall;

        if (sockets.indexOf(socket) < 0) {
            console.log('connection socket:'+socket);
            dbCall = true;
            sockets.push(socket);
        }

        socket.join(socket);

        socket.on('disconnect', function () {
            if (sockets.indexOf(socket) >= 0) {
                console.log('disconnect socket:'+socket);
                sockets.splice(sockets.indexOf(socket), 1);
            }
            socket.leave(socket.room);
        });

        socket.on('getUserData', function (gender) {
            console.log('getUserData:'+gender);
            if (dbCall) {
                dbCall = false;
                db.getUser(gender, function(result){
                    if (result != null) {
                        io.sockets.in(socket).emit('users', result);
                    }
               });
           }
       });

       socket.on('getUsers', function () {
          dbCall = true;
      });

       socket.on('select', function (user) {
           db.voteUser(user.id, function(result){
               if (result != null) {
                   console.log(JSON.stringify(result));
                   getAllData();
               }
          });
       });

       socket.on('message', function (data) {
           console.log(JSON.stringify(data));
           db.setComment(data.id, data.message, function(result){
               if (result != null) {
                   console.log(JSON.stringify(result));
                   getAllData();
               }
          });
       });

    });
};

exports.getAllData = function(req, res, io) {
    db.getAllData(function(result){
          if (result != null) {
            console.log(result);
            res.send(result);
          }
     });
};

function getAllData() {
    db.getAllData(function(result){
        if (result != null) {
            broadcast('allusers', result);
        }
   });
}

function updateRoster() {
  async.map(
    sockets,
    function (socket, callback) {
      socket.get('name', callback);
    },
    function (err, names) {
      broadcast('roster', names);
    }
  );
}

function broadcast(event, data) {
    console.log('sockets:'+sockets.length);
  sockets.forEach(function (socket) {
    socket.emit(event, data);
  });
}

