var Sequelize = require('sequelize');
var sequelize = new Sequelize('worldcup', 'root');

var User = sequelize.define('user', {
    name: {type: Sequelize.STRING, allowNull: false},
    pic: Sequelize.STRING,
    desc: Sequelize.TEXT,
    gender: {type: Sequelize.STRING,  defaultValue: 'M'}
});

var Comment = sequelize.define('comment', {
    user_id: {type: Sequelize.INTEGER },
    comment: {type: Sequelize.STRING }
});

module.exports.getAllUser = function(callback){
    User.findAll().then(callback);
}

module.exports.getUser = function(gender, callback){
    User.findAll({where : {gender : gender}}).then(callback);
}

module.exports.setUser = function(device_id, email, phone, lat, lng, callback){
    sequelize.sync().then(function() {
        return User.create({
                        device_id: device_id,
                        email: email,
                        phone: phone,
                        lat: lat,
                        lng: lng,
                        }).then(callback);
      });
}

module.exports.voteUser = function(id, callback){
    sequelize.sync().then(function() {
        return Comment.create({
                        user_id: id
                        }).then(callback);
    });
}

module.exports.setComment = function(id, message, callback){
    sequelize.sync().then(function() {
        return Comment.create({
                        user_id: id,
                        comment : message
                        }).then(callback);
    });
}

module.exports.getAllData = function(callback){
    User.findAll().then(function(userData) {
        if (userData != null) {
            Comment.findAll().then(function(commentData) {
                if(commentData != null) {
                    var voteCnt = 0;
                    var commentCnt = 0;
                    for(var i in userData) {
                        userData[i].dataValues.comments = [];
                        userData[i].dataValues.votes = [];
                        for(var j in commentData) {
                            if (userData[i].id == commentData[j].user_id) {
                                if (commentData[j].comment == null) {
                                    userData[i].dataValues.votes.push(commentData[j]);
                                } else {
                                    userData[i].dataValues.comments.push(commentData[j]);
                                }
                            }
                        }
                    }
                    callback(userData);
                }
            });
        }
    });
}
