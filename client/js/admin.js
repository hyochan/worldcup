var socket = io.connect();

function AdminController($scope) {
    $scope.users = [];
    $scope.comments = [];
    $scope.name = '';
    $scope.text = '';

    socket.on('select', function (comment) {
          $scope.comments.push(comment);
          $scope.$apply();
    });

    socket.on('connect', function () {
      $scope.setName();
    });

    socket.on('allusers', function (data) {
      data.sort(function (b, a) {
      	return a.votes.length < b.votes.length ? -1 : a.votes.length > b.votes.length ? 1 : 0;
      });

      $scope.users = data;
      $scope.$apply();
    });

    socket.on('roster', function (names) {
      $scope.roster = names;
      $scope.$apply();
    });

    $scope.send = function send() {
      console.log('Sending message:', $scope.text);
      socket.emit('message', $scope.text);
      $scope.text = '';
    };

    $scope.setName = function setName() {
      socket.emit('identify', $scope.name);
    };

    $scope.commenthoverIn = function(){
        this.hoverNode = true;
    };

    $scope.commenthoverOut = function(){
        this.hoverNode = false;
    };
}

$(function(){
    $('#btn_male').click(function(){
      $('.popup').hide();
      $('.container').show();
      socket.emit('getUserData', 'M');
    });

    $('#btn_female').click(function(){
        $('.popup').hide();
        $('.container').show();
        socket.emit('getUserData', 'F');
    });
});


