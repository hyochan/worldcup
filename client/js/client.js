var socket = io.connect();

function ClientController($scope) {
    $scope.users = [];
    $scope.roster = [];
    $scope.name = '';
    $scope.text = '';

    var game_users = new Array();
    var select_users = new Array();
    var total;
    $scope.max_game;
    $scope.cnt_game;
    var player1, player2;

    socket.on('connect', function () {
      $scope.setName();
    });

    socket.on('users', function (data) {
        socket.emit('getUsers');
        $scope.users = [];
        game_users = data;

        $scope.max_game = game_users.length / 2;
        $scope.cnt_game = 0;
        showPlayer();

        $scope.roster = data;
    });

    function showPlayer(){
        $scope.users = [];

        total = game_users.length;

        if($scope.max_game < 1) {
            alert(JSON.stringify(select_users));
            $scope.users = select_users;
            $scope.$apply();
            return;
        }

        player1 = Math.floor(Math.random() * total);
        player2 = Math.floor(Math.random() * total);

        if (player1 != player2) {
            $scope.users.push(game_users[player1]);
            $scope.users.push(game_users[player2]);
            $scope.$apply();
        } else {
            showPlayer();
        }
    }

    $scope.selectPlayer = function(id){
        if($scope.max_game < 1) {
            return;
        }

        if (game_users[player1].id == id) {
            select_users.push(game_users[player1]);
            socket.emit('select', game_users[player1]);
        } else {
            select_users.push(game_users[player2]);
            socket.emit('select', game_users[player2]);
        }
        $scope.cnt_game++;
        //문제 인듯..
        game_users.splice(player1, 1);
        game_users.splice(player2, 1);

        if ($scope.max_game < $scope.cnt_game + 1) {
            game_users = select_users;
            $scope.max_game = game_users.length / 2;
            $scope.cnt_game = 0;
            $scope.roster = game_users;
            if($scope.max_game >= 1) {
                select_users = new Array();
            }
        }
        showPlayer();
    }

    $scope.send = function send(id) {
      var data = {
                  'id' : id,
                  'message' : this.text
                  };
      socket.emit('message', data);
      this.text = '';
    };
}

$(function(){
    $('#btn_male').click(function(){
      $('.popup').hide();
      $('.client').show();
      socket.emit('getUserData', 'F');
    });

    $('#btn_female').click(function(){
        $('.popup').hide();
        $('.client').show();
        socket.emit('getUserData', 'M');
    });

});


